#!/usr/bin/env python3

import time
import imaplib
import datetime
import argparse
import configparser

today = datetime.date.today()
maxRetries = 10

def getQuery(max_days):
    cutoff_date = today - datetime.timedelta(days=max_days)
    before_date = cutoff_date.strftime('%d-%b-%Y')
    query = '(BEFORE "%s")' % before_date
    print("query: {}".format(query))
    return query

def start(host, port, login, password):
    print("authorizing")
    imap = imaplib.IMAP4_SSL(host, port, timeout=3600)
    imap.login(login, password)
    return imap

def search(imap, query):
    typ, data = imap.search(None, 'ALL', query)
    msgs = data[0].split()
    print("search: {}. found: {} msgs".format(typ, len(msgs)))
    return msgs

def mark(imap, msgs):
    for num in msgs:
        print("setting '\Deleted' flag to the msg {}/{}".format(int(num), len(msgs)))
        imap.store(num, '+FLAGS', '\\Deleted')

def remove(imap):
    print("permanently removing msgs")
    imap.expunge()

def process(section, cfg, retry=0):
    query = getQuery(int(cfg['days']))
    mboxes = [x.strip() for x in cfg['mbox'].split(',')]
    try:
        imap = start(cfg['host'], cfg['port'], cfg['user'], cfg['pass'])
    except:
        print("ERROR: cannot connect to the {} IMAP".format(section))
        if retry >= maxRetries:
            return
        time.sleep(10)
        retry += 1
        process(section, cfg, retry)
        return
    for mbox in mboxes:
        print("[{}] {}".format(section, mbox))
        try:
            imap.select(mbox)
            msgs = search(imap, query)
            if len(msgs) > 0:
                mark(imap, msgs)
                remove(imap)
            print("closing mailbox '{}'".format(mbox))
            imap.close()
        except:
            print("ERROR: cannot process the {} mailbox '{}'".format(section, mbox))
    print("closing connection to the IMAP server")
    imap.logout()

def parseConfig(file):
    print("reading config file '{}'".format(file))
    config = configparser.RawConfigParser()
    config.read(file)
    return config

def parseArgs():
    parser = argparse.ArgumentParser()
    parser.add_argument("-c", "--config", dest="config", help="config file path", default="config.ini", metavar="FILE")
    return parser.parse_args()

if __name__ == "__main__":
    args = parseArgs()
    config = parseConfig(args.config)
    for section in config.sections():
        print("\n[{}]".format(section))
        process(section, config[section])
