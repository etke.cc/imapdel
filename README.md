# imapdel

a simple CLI utility to remove old messages from your email.

## Usage

### 1. create a config file

> default config file name: `config.ini`

```toml
[name]
host = imap.google.com
port = 993
user = your@email.com
pass = YourSuper3Qure!!!Passwrod
mbox = Sent, Archive
days = 365
```

* **name** - user-friendly name, eg `work`, `personal`, etc.
* **host** - IMAP host
* **port** - IMAP TLS port
* **user** - IMAP username (usually email address is username, but depends on provider)
* **pass** - IMAP password
* **mbox** - Mailboxes (dirs) list to cleanup, separated by comma, eg `INBOX,Archive,Spam`, etc.
* **days** - TTL, emails older than that value will be removed

### 2. usage

```bash
usage: imapdel.py [-h] [-c FILE]

options:
  -h, --help            show this help message and exit
  -c FILE, --config FILE
                        config file path
```

## systemd unit

Example systemd unit, assuming you will `chmod +x imapdel.py`, put it into `$HOME/bin` and put config file into `$HOME/.config`:

```
[Unit]
Description=imapdel
Documentation=https://gitlab.com/etke.cc/tools/imapdel
After=network-online.target

[Service]
Type=oneshot
ExecStart=/bin/sh -c 'exec ${HOME}/bin/imapdel.py -c ${HOME}/.config/imapdel.ini'

[Install]
WantedBy=default.target
```
